package com.example.running_background;


import java.util.TimerTask;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class MyService extends IntentService{

	private NotificationManager mNM;
	Bundle b;
	Intent notificationIntent;
	
	
	public MyService() {
		super("MyService");
		
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub
		
		
		
		 
        try
        {
                 
          	mNM = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);       	
        	notificationIntent = new Intent(this, MainActivity.class);
        	
        	
          	showNotification();
        }
        catch(Exception ex)
        {
        	Log.v("esty2", ex.getMessage());
			
        	
        }
        
	}
	
public void showNotification() {
	
		
		try
		{
			CharSequence text ="Test";

			Notification notification = new Notification(
					R.drawable.ic_launcher, text,
					System.currentTimeMillis());
			PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
					new Intent(this, MainActivity.class), 0);
			notification.setLatestEventInfo(this, "roni", "wer are testing", contentIntent);
			notification.flags = Notification.FLAG_ONGOING_EVENT
					| Notification.FLAG_NO_CLEAR;
			notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_SINGLE_TOP);

			mNM.notify(R.string.app_name, notification);
		}
		catch(Exception ex)
		{
			Log.v("roni", ex.getMessage());
			
		}
		
	}



class MyTimerTask extends TimerTask {

	@Override
	public void run() {
		
				showNotification();
			
			}
	
		}

}
