package com.roni.country;

import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

class GridAdapter extends BaseAdapter 
{
	
	List<CountryEnity>list;
	Context context;

	public GridAdapter(Context context, List<CountryEnity> list)
	{
		this.list=list;
		this.context=context;
		

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		View v=convertView;
		
		try
		{
			if(v==null)
			{
				LayoutInflater inflater=(LayoutInflater)context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
				v=inflater.inflate(R.layout.row,null);
			}
			
			
			TextView lblRank=(TextView)v.findViewById(R.id.lblRank);
			TextView lblPopulation=(TextView)v.findViewById(R.id.lblPopulation);
			TextView lblCountry=(TextView)v.findViewById(R.id.lblCountry);
			
			lblRank.setText(list.get(position).Rank);
			lblPopulation.setText(list.get(position).Population);
			lblCountry.setText(list.get(position).Country);
			
			
		}
		catch(Exception ex)
		{
			Log.v("abc", ex.getMessage());
		}
		
		
		return v;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return list.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

}