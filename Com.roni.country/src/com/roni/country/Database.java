package com.roni.country;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class Database {

	//connection handle
	public static SQLiteDatabase db ;
	private DatabaseHelper DBHelper;
	public Context contxt = null;
	
	//database info
	public static final String db_name = "CountryDatabase.db";
	public static final int db_version = 1;
	
	//table info
	public static final String Table_Name = "Country";
	public static final String Sl_no = "id";
	public static final String Rank = "rank";
	public static final String Country = "country";
	public static final String Population = "population";
	public static final String tag = "Database";
	
	
	private static final String CREATE_Database = "CREATE TABLE "+Table_Name+" ( "+Sl_no + " INTEGER PRIMARY KEY AUTOINCREMENT, " +Rank + " text, " +Country + " text , "+Population + " text) ";	
	public static final String DROP_Table = "DROP TABLE IF EXISTS "+Table_Name;
	
	
	public Database(Context ctx)
	{
		this.contxt = ctx;
		DBHelper = new DatabaseHelper(contxt);
	}
	
	private static class DatabaseHelper extends SQLiteOpenHelper{
		
		DatabaseHelper(Context context){
			super(context, db_name, null, db_version);
		}
		@Override
		public void onCreate(SQLiteDatabase db) {
			// TODO Auto-generated method stub
			
			try {
				db.execSQL(CREATE_Database);
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// TODO Auto-generated method stub
			Log.w(tag,"Upgrading database from version" + oldVersion + " to " + newVersion + ", which will destroy all old data");
			db.execSQL(DROP_Table);
			onCreate(db);
		}
	}
	
	public Database open() throws SQLException
	{
		db = DBHelper.getWritableDatabase();
		return this;
	}
	
	public void close(){
		DBHelper.close();
	}
	
	public long insertRecord(String rank, String country, String population)
	{
		ContentValues initialValues = new ContentValues();
		initialValues.put(Rank, rank);
		initialValues.put(Country, country);
		initialValues.put(Population, population);
		return db.insert(Table_Name, null, initialValues);
	}
	
	public Cursor getAllHistory()
	{ 
		return db.query(Table_Name, new String[] { Sl_no, Rank, Country, Population}, null, null, null, null, null, null);
	}	
}
