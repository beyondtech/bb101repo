package com.roni.country;



import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class History extends Activity  {

	
	Database db = new Database(this);
	 ListView listView ;
	 private ArrayList<String> results = new ArrayList<String>();
	 List<CountryEnity>list=new ArrayList<>();
	 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.history);
		
		  
        listView = (ListView) findViewById(R.id.list);
        
        
        
        db.open();
		Cursor c = db.getAllHistory();
		if(c.moveToFirst())
		{
			while(c.moveToNext()){
				
				do {
					String rank = c.getString(c.getColumnIndex("rank"));
    				String con = c.getString(c.getColumnIndex("country"));
    				String pop = c.getString(c.getColumnIndex("population"));
    				
    				Log.v("esty", pop);
    				CountryEnity e=new CountryEnity();
    				e.Rank=rank;
    				e.Population=pop;
    				e.Country=con;
    				list.add(e);
    				
    				
    		
    			}while (c.moveToNext()); 
			}
		}

		GridAdapter adapter=new GridAdapter(getApplicationContext(), list);
	


        // Assign adapter to ListView
        listView.setAdapter(adapter); 
        
    }
	
		
		
		
}
