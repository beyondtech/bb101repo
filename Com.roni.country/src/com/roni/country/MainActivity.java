package com.roni.country;
import android.R.string;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener {

	
	Button btnSave, btnShow;
	EditText txtrank, txtcountry, txtpop;
	
	Database db = new Database(this);
	
	
	public static int count = 0;
	public static String list = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		
		btnSave = (Button) findViewById(R.id.save);
		btnShow = (Button)findViewById(R.id.show);
		txtrank = (EditText)findViewById(R.id.rank);
		txtcountry = (EditText)findViewById(R.id.country);
		txtpop = (EditText)findViewById(R.id.pop);
		btnSave.setOnClickListener(this);
		btnShow.setOnClickListener(this);

		
	}


	@SuppressWarnings("unused")
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
		if(v.getId() == R.id.save)
		{
			
			String r = null; 
			String c = null;
			String p = null;	
				
				r= txtrank.getText().toString();
				c= txtcountry.getText().toString();
				p= txtpop.getText().toString();
				
				
				db.open();
				db.insertRecord(r, c, p);
				
				Context context1 = getApplicationContext();
				CharSequence text = "Inserted!";
				int duration1 = Toast.LENGTH_SHORT;

				Toast toast = Toast.makeText(context1, text, duration1);
				toast.show();
				Log.d("main","data inserted!!");
			}
			
			
		
		
		
		if(v.getId() == R.id.show)
		{
			try{
				
					Intent i = new Intent(MainActivity.this,History.class);
					
	    
	    			startActivity(i);
	    			Log.d("main","Intent Called");
	    			
	    		
				}catch (Exception e) {
					
					e.printStackTrace();
				}
		}
		
		
	}
}
    

